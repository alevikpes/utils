"""Use it as a decorator for any python function or method."""
from time import time


def timeit(func):
    """Measures and prints out the execution time for the decorated function.
    """
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        print(f'{func.__name__} executed in {end - start:.4f} seconds')
        return result

    return wrapper
