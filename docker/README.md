
# Docker Engine and Docker Compose setup

I use the installation from the Docker's `apt` repository
[here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
on the [official website](https://docs.docker.com/engine/install/ubuntu/).

It also installs `Docker Compose`.

Use also
[this post-install instruction](https://docs.docker.com/engine/install/linux-postinstall/)
to be bale to run docker commands without using `sudo`.


### Build the *Docker* image of your repo
Clone the repo in the directory of your choice
```
git clone git@gitlab.company.com:project/repo.git
```
and `cd` to that directory
```
cd /your/dir/repo
```

Now you need to login to the registry with your *GitLab* credentials:
```
docker login registry.company.com
```

Then run
```
docker compose build
```
When it is finished, your image is ready.


### Start your environment
Start *Docker* instance
```
docker compose up
```
When it is running, start the *Django* server locally on port 8000
```
docker exec -it contaner_name python manage.py runserver 0.0.0.0:8000
```


### Set up your application
Update your migrations
```
docker exec -it container_name python manage.py migrate
```

Create a user
```
docker exec -it container_name python manage.py createsuperuser
```
Make an email and a password for the user, which you are going to use to login
to the application. Can be fake.



## Tips and tricks

### Remove `*.pyc` files created by root in the docker

```bash
sudo rm $(find . -type f -name "*.pyc")
```


### Run pdb in the host terminal on a python code inside the container

Many times it is not possible to debug your python code running inside
a container from the host terminal. Adding these two lines to your
python service may help with it:
```yaml
services:
  your_service:
    ...
    stdin_open: true
    tty: true
```
