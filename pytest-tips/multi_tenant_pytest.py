"""
This code allows to use `pytest` and `pytest-factoryboy` modules in their
full power.

Useful functionality for projects with multi tenant databases, if the
projects using one of these modules:
- django-tenant-schemas: https://github.com/bernardopires/django-tenant-schemas
or its fork https://github.com/gxavier-pland/django-tenant-schemas:

from tenant_schemas.utils import get_public_schema_name, tenant_context

- django-tenants: https://github.com/django-tenants/django-tenants:

from django_tenants.utils import get_public_schema_name, tenant_context

This is an adapted code from this gist:
https://gist.github.com/GeeWee/54b6fd7ad87bcc876781ae02d1e0993d

Add this code to the `conftest.py` file and keep the fixtures' names,
such that it can overwrite the default `run_in_tenant_context` and
`db` fixtures.
"""

# Import your tenant model here, if you have it. Otherwise use the default
# from the module.
from my_project.tenant.models import Tenant


# Config for test tenant
TEST_TENANT_DOMAIN_URL = 'test_tenant.com'
TEST_TENANT_NAME = "test_tenant"
TEST_TENANT_SCHEMA_NAME = "test_tenant_schema"


def _get_or_create_tenant(name, domain_url, schema_name) -> Tenant:
    """A function that gives us a test_tenant if we need it."""
    try:
        tenant = Tenant.objects.get(schema_name=schema_name)
        logger.debug("Using previously created tenant")
    except Tenant.DoesNotExist:
        logger.debug("Creating new test tenant")
        tenant = Tenant(
            name=name,
            domain_url=domain_url,
            schema_name=schema_name,
        )
        # This saves the tenant and creates the tenant schema.
        tenant.save(verbosity=0)

    return tenant


@pytest.fixture(scope="function", autouse=True)
def run_in_tenant_context(db, request, caplog):
    # Here we create the statics
    logger.info("Running in test tenant context")
    t = _get_or_create_tenant(
        name=TEST_TENANT_NAME,
        domain_url=TEST_TENANT_DOMAIN_URL,
        schema_name=TEST_TENANT_SCHEMA_NAME,
    )
    with tenant_context(t):
        # Delete unknown offloading ServiceType from datamigration
        # Create statics if needed
        if "no_statics" not in request.keywords:
            # We don't want to show setup logs normally
            with caplog.at_level(logging.WARNING):
                logger.info("Populating static tables")
        yield


@pytest.fixture(scope="function")
def db(request, django_db_setup, django_db_blocker):
    """Django db fixture.

    Seeing as this has the same signature, it overrides the fixture
    in `pytest_django.fixtures`.

    It doesn't support quite the same things, such as transactional tests
    or resetting of sequences.

    The way our setup works here, is that due to performance reasons
    we want to override this, and then create our public and test tenant,
    before entering into the `atomic` block that pytest and Django normally
    runs tests in - this way we only have to do the heavy work of
    migrating our schemas once every test-run, rather than every test.
    """
    logger.info("Fetching the DB fixture")

    # Some weird django db_blocker magic
    django_db_blocker.unblock()
    request.addfinalizer(django_db_blocker.restore)

    # Create the public and the test tenant.
    # We do this right before the pre_setup so it doesn't
    # get axed by the atomic block()
    Tenant.objects.get_or_create(schema_name=get_public_schema_name())
    _get_or_create_tenant(
        name=TEST_TENANT_NAME,
        domain_url=TEST_TENANT_DOMAIN_URL,
        schema_name=TEST_TENANT_SCHEMA_NAME,
    )

    """Here we distinguish between a transactional test or not
    (corresponding to Djangos TestCase or its TransactionTestCase).
    Some tests that create/drop tenants can't be run
    inside an atomic block, so must be marked as transactional.
    """
    if "transactional" in request.keywords:
        test_case = TransactionTestCase(methodName="__init__")
        logger.debug("Using transactional test case")
    else:
        # This performs the rest of the test in an atomic()
        # block which will roll back the changes.
        logger.debug("Using regular test case")
        test_case = TestCase(methodName="__init__")

    test_case._pre_setup()
    # Post-teardown function here reverts the atomic
    # blocks to leave the DB in a fresh state.
    # This rolls the atomic block back
    request.addfinalizer(test_case._post_teardown)
