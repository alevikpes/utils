from contextlib import contextmanager


@contextmanager
def raises_exception(ExpectedException):
    """This context manager validates to `True` if the `ExpectedException`
    is raised and notifies if another exception is raised.

    This is convenient to use in the parametrised tests. See usage in the
    example bellow.
    """
    try:
        yield
    except ExpectedException as ex:
        pass
    except Exception as ex:
        raise AssertionError(f"An unexpected exception is raised:\n{repr(ex)}")


### USAGE ###

import pytest
from rest_framework.exceptions import ValidationError

from myapp.serializers import OIDCConfigurationSerializer


class TestOIDCConfigurationSerializer:
    @pytest.mark.parametrize(
        "data, ex",
        (
            ({"client_id": "", "client_secret": ""}, ValidationError),
            ({"client_id": None, "client_secret": None}, ValidationError),
            ({"client_id": "some-id", "client_secret": ""}, ValidationError),
            ({"client_id": "", "client_secret": "some-secret"}, ValidationError),
            ({"client_id": "some-id", "client_secret": "some-secret"}, None),
        ),
    )
    def test_exceptions(self, data, ex):
        s = OIDCConfigurationSerializer(data=data)
        with raises_exception(ex):
            s.is_valid(raise_exception=True)
