import builtins

import mock
import pytest


class TestCLIInput:

    @staticmethod
    def test_cli_input():
        # Mock the built-in `input` function
        with mock.patch.object(
            builtins,
            'input',
            lambda _: 'some input',
        ):
            # Assert the return value
            assert input('Insert your input: ') == 'some input'
