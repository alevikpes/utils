# conftest.py
import pytest
from django.contrib.auth.models import AnonymousUser


# make AnonymousUser() a fixture
@pytest.fixture
def anonymous_user():
    return AnonymousUser()


@pytest.fixture
def normal_user(user_factory):
    return user_factory.build(**{
        'username': 'normaluser',
        'password': 'aabbccddeeffgg',
    })


@pytest.fixture
def admin(user_factory, organization_factory):
    return user_factory.build(**{
        'username': 'admin',
        'password': 'aabbccddeeffgg',
        'is_admin': True,
    })


@pytest.fixture
def superadmin(user_factory):
    return user_factory.build(**{
        'username': 'superadmin',
        'password': 'aabbccddeeffgg',
        'is_superadmin': True,
    })


## parametrized fixture for different users
#@pytest.fixture(
#    params=[
#        AnonymousUser,
#        'normal_user',
#        'admin',
#        'superadmin',
#    ],
#    ids=[
#        'anonymous_user',
#        'normal_user',
#        'admin',
#        'superadmin',
#    ],
#)
#def parametrised_user(request):
#    return request.param


# test_your_view.py
import pytest
from django.test import RequestFactory

from your.app.views import YourView


class SystemUserCreateViewTests:
    """
    Unit tests for view `admin_views.SystemUserCreateView`.
    """

    @pytest.mark.parametrize(
        'user',
        (
            'anonymous_user',
            'normal_user',
            'admin',
            'superadmin',
        ),
    )
    def test_only_accessible_by_staff_superusers(self, request, user):
        abs_url = '/your/url/'
        mock_request = RequestFactory()
        mock_request.user = request.getfixturevalue(user)
        #response = mock_request.get(abs_url)
        response = YourView.as_view()(mock_request)
        # assert your response
        assert response is expected_response
