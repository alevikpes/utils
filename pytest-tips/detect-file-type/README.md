
### Detecting file type based on its signature

Detecting file type based on the extension is not very reliable method. The
better way is to check the file signature and detect the MIME type of the file.

These are the usefull resources:
https://www.iana.org/assignments/media-types/media-types.xhtml
https://en.wikipedia.org/wiki/List_of_file_signatures
https://mimesniff.spec.whatwg.org/#understanding-mime-types

##### Testing
Testing of the MIME type may look like this:
```python
import pytest

from django.core.files.uploadedfile import SimpleUploadedFile


FILE_DATA = (
    ('application/pdf', bytearray(b'\x25\x50\x44\x46\x2d'), True),
    # any file with normal text is considered as `text/plain`
    # the most common headers are:
    ('text/plain', bytearray(b'\xEF\xBB\xBF'), True),
    ('text/plain', bytearray(b'\xfe\xff'), True),
    ('text/plain', bytearray(b'\xff\xfe'), True),
    ('text/xml', bytearray(b'\x3C\x3F\x78\x6D\x6C'), False),
    # older doc, xls, ppt etc.
    ('application/msword', bytearray(b'\xD0\xCF\x11\xE0\xA1\xB1\x1A\xE1'),
     True),
    ('rtf', bytearray(b'7B\x5C\x72\x74\x66\x31'), True),
    # also all modern ms office types are recognised as zip files
    ('application/zip', bytearray(b'\x50\x4B\x03\x04'), True),
    ('application/zip', bytearray(b'\x50\x4B\x05\x06'), True),
    ('application/zip', bytearray(b'\x50\x4B\x07\x08'), True),
    ('application/7z', bytearray(b'\x37\x7A\xBC\xAF\x27\x1C'), True),
    ('application/x-tar', bytearray(b'\x75\x73\x74\x61\x72\x00\x30\x30'),
     True),
    ('application/x-gtar', bytearray(b'\x75\x73\x74\x61\x72\x20\x20\x00'),
     True),
    # exe: application/octet-stream, application/x-msdownload
    ('application/octet-stream', bytearray(b'\x4D\x5A'), False),
    ('application/x-msdownload', bytearray(b'\x4D\x5A'), False),
    ('image/png', bytearray(b'\x89\x50\x4E\x47\x0D\x0A\x1A\x0A'), False),
)


@pytest.mark.parametrize('content_type,file_content,validation',
                          FILE_DATA)
def test_upload_file(self, content_type, file_content, validation):
    test_file = SimpleUploadedFile('test-file', file_content, content_type)
    # do some test
```
