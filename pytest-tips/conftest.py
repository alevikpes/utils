import pytest


logger = logging.getLogger('conftest')


# remove limitation on the test output
import unittest
unittest.TestCase.maxDiff = None


# Uncomment the following block in case of settings problems
## Configure settings for project
## Need to run this before calling models from application!
#import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "my_project.settings")
## Import settings
#import django
#django.setup()
###########


# Add fixtures files as plugins.
# NOTE: this only works for files called `fixtures.py`.
pytest_plugins = (
    "app1.tests.fixtures",
    "app2.tests.fixtures",
)
