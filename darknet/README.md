# DARKNET


**Darknet** is an open source neural network framework written in C and
CUDA. See more at the [darknet web page](https://pjreddie.com/darknet/).

When I was searching for instructions about installation and usage, I found
[this github project](https://github.com/AlexeyAB/darknet), but I could not
make it working with python and discovered a
[great repo of Stephane Charette](https://github.com/stephanecharette),
which has lots of tools for executing darknet projects. There are lots of
instructions and tutorials on [his web page](https://www.ccoderun.ca/).
There is also a discord server with many helpful topics and people.

So, my darknet experience started from
[this page](https://www.ccoderun.ca/programming/2019-08-18_Installing_and_building_Darknet/).
But I immediately had to leave it for the CUDA and cuDNN installation, since
I had an NVIDIA GTX-1060 graphics card and could use it for speeding up
my work.

>
>**NOTE**
> I use Ubuntu 20.04 as my main OS and this document is completely made for it.
>


### Installing CUDA and cuDNN

##### Installing NVIDIA Driver
This usually starts from the installation of the latest NVIDIA driver.
If you already have the driver installed, skip this step.

The installation of the driver can be done in a couple of ways.

There is GUI tool **Software & Updates**, which has a separate tab
**Additional Drivers**, where you can choose your prefered driver to
install. Simply, choose the one and click **Apply Changes**. After a short
time the driver will be installed. You may need to restart your system
after this, just to make sure, that the changes have taken effect.

The other way is via command line interface. It may look inconvenient
for somebody, who is not familiar with it, but it is useful in certain
cases. Here is a short instruction, but you may need to search online
for details.

Then you need to switch to one of the `tty`'s by using
`CTRL+ALT+<one of the Fxx keys>`, login there, stop your X server and install
the chosen driver. Search online about how to stop your GUI server. The
default command (it also worked for me) is:
```bash
gdm stop
```
If this command did not work for you, try one of the following:
```bash
sudo service gdm stop
# or
sudo service lightdm stop
# or
sudo service kdm stop
```
After the previous command successfully executed, you can see the list
of available drivers for your GPU by running the following command in
your terminal:
```bash
ubuntu-drivers devices
```
It will display a list of drivers, where you can choose the latest one
for your GPU (with the highest number). For me it was `nvidia-driver-495`.

Installation of the driver is done by simply running:
```bash
sudo apt install nvidia-driver-<your chosen version>
```
After this reboot your system. The system should start normally.


##### Installing CUDA
Installation of CUDA is described on the
[official NVIDIA page](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#abstract).
I repeat here those instructions for my case, which must be default for most
of you.

Install a `g++` compiler on your system:
```bash
sudo apt install g++
```
It is, probably, already installed.

Download the binary from
[here](https://developer.nvidia.com/cuda-downloads?target_os=Linux).

Read carefully and perform all the checks described in the
[Pre-installation Actions section](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#pre-installation-actions)
of the document. All of it was default on my OS.

I chose for the local installer `deb (local)` and followed the given
instructions:
```bash
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600

# The following command downloads a large CUDA repository and takes a long time
wget https://developer.download.nvidia.com/compute/cuda/11.5.0/local_installers/cuda-repo-ubuntu2004-11-5-local_11.5.0-495.29.05-1_amd64.deb

# setup the repository public key
sudo apt-key add /var/cuda-repo-ubuntu2004-11-5-local/7fa2af80.pub

# Install the downloaded repository
sudo dpkg -i cuda-repo-ubuntu2004-11-5-local_11.5.0-495.29.05-1_amd64.deb

# Install CUDA
sudo apt-get update
sudo apt-get -y install cuda
```
In short, this sets up an NVIDIA repository on your system and allows to use
your package manager to install CUDA. This took me around 1.5 hours, since
my internet connection was not very fast.

##### Installing cuDNN
**cuDNN** is a special package, which allows using NVIDIA GPU's for
Deep Learning tasks. Downloading of the binary is only possible via a
registration in the NVIDIA dev program. I did not want to register and
found [this page](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#package-manager-ubuntu-install).
It describes a procedure of installing cuDNN without registration. It is
similar to the installation of CUDA described previously:
```bash
# setup some variables
export OS=ubuntu2005
export cudnn_version=8.3.0.*
export cuda_version=cuda11.5

# setup repository
wget https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/cuda-${OS}.pin
sudo mv cuda-${OS}.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/7fa2af80.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/ /"
sudo apt-get update

# install cuDNN
sudo apt-get install libcudnn8=${cudnn_version}-1+${cuda_version}
sudo apt-get install libcudnn8-dev=${cudnn_version}-1+${cuda_version}
```
This installation also took around 1 hour, due to slow internet speed.

##### Finishing Setup
The last step in the NVIDIA-related tasks is adding the CUDA paths
to the Linux PATH. It is necessary to include the following 2 lines into
your `.bashrc` or another bash configuration file:
```bash
export PATH=/usr/local/cuda-11.5/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-11.5/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```

##### Recommended Actions
[NVIDIA official guide](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#recommended-post)
recommends some extra actions after the installation.

I skip them for now.

##### Verification
There are a couple of ways of verification of the CUDA installation.

The [official CUDA guide](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#verify-installation)
and the [official cuDNN guide](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#verify)
have verification procedures.

[This guide](https://prashant-rawat.medium.com/install-cuda-11-0-and-cudnn-on-ubuntu-20-04-7a1a4063537b)
uses python and tensorflow for verification.

[Another guide](https://medium.com/geekculture/installing-cudnn-and-cuda-toolkit-on-ubuntu-20-04-for-machine-learning-tasks-f41985fcf9b2)
uses a simple test command to test the CUDA installation before the
official verification:
```bash
nvidia-smi
```
It worked as described, so I stopped with verification at this point.


### Installing and Building Darknet

Everything here I start from
[this tutorial](https://www.ccoderun.ca/programming/darknet_faq/#how_to_build_on_linux).

The first step is installing missing libraries:
```bash
sudo apt-get install build-essential git libopencv-dev
```
Create a directory for your project, `cd` there and clone the
[AlexeyAB darknet repo](https://github.com/AlexeyAB/darknet):
```bash
git clone https://github.com/AlexeyAB/darknet.git
cd darknet
```

###### Build the CPU version of darknet
Before building darknet, change the required parameters in the Makefile
```bash
GPU=0
CUDNN=0
CUDNN_HALF=0
OPENCV=1
AVX=1
OPENMP=1
LIBSO=1
ZED_CAMERA=0
ZED_CAMERA_v2_8=0
```
Save it and run:
```bash
make
```
This process will create some extra files. copy those file into your local
directories:
```bash
sudo cp libdarknet.so /usr/local/lib/
sudo cp include/darknet.h /usr/local/include/
```
Then run:
```bash
sudo ldconfig
```
