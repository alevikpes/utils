#!/bin/bash -e

# Replace `DEB` with `RPM` in `CM_package.cmake`.

# For opencv read this https://idroot.us/install-opencv-fedora-40/ or another
# related article.

# Install dependencies:
echo "Installing dependencies ..."
sudo dnf group install -y c-development development-tools
sudo dnf install -y \
    rpm-build \
    tclap \
    file-devel \
    file-libs
echo "Done"

echo "Building darknet ..."
mkdir -p build
cd build

# Pick just 1 of the following -- either Release or Debug
set BUILD_TYPE=Release
#set BUILD_TYPE=Debug

cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
make -j $(nproc)
make package

echo "Done"

echo Make sure you install the .rpm file:
ls -lh *.rpm

#pack=$(ls -lh *.rpm)
#echo "Installing $pack ..."
#sudo dnf install $pack

echo "Done"

cd ..
