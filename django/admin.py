from django.contrib import admin
from django.urls import resolve, reverse
from django.utils.html import format_html_join

from course.models import Course


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):

    list_display = [
        'title',
        'teacher',
        '_documents',
    ]
    search_fields = [
        'title',
        'teacher__username',
        'document__file_name',
    ]

    @admin.display(description='Documents')
    def _documents(self, obj):
        # Use `resolve` to get the correct url and kwargs:
        #print(resolve('/admin/document/document/15/change/'))
        docs = obj.document.all()
        docs_details = {}
        for doc in docs:
            doc_url = reverse(
                'admin:document_document_change',
                kwargs={'object_id': doc.pk},
            )
            docs_details.update({doc.file_name: doc_url})

        return format_html_join(
            '\n',
            '<li><a href="{}">{}</a></li>',
            ((dv, dk) for dk, dv in docs_details.items()),
        )
