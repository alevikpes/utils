import logging
import time

import vlc
from pytube import YouTube


logger = logging.getLogger(__name__)


class BaseYT:

    def __init__(self, url: str):
        self.url = url
        self.yt = self._get_video_obj()

    def _get_video_obj(self):
        return YouTube(self.url)

    def show_mime_types(self) -> list:
        return [s.mime_type for s in self.yt.streams]

    def get_mime_type(self, mime_type_str: str) -> list:
        return [s for s in self.yt.streams if s.mime_type == mime_type_str]

    def play_stream(self):
        strm = self.yt.streams.first()
        media = vlc.MediaPlayer(strm.url)
        media.play()
        print(f'{media.get_state()}')
        while media.is_playing:
            if media.get_state() == vlc.State.Ended:
                media.stop
                break

        print(f'{media.get_state()}')


if __name__ == '__main__':
    #url = 'https://www.youtube.com/watch?v=41-dLH5YqeU'  # Rhodesians Never Die
    #url = 'https://www.youtube.com/watch?v=h_s7L0vrjOI'  # Rhodies Everywhere
    url = 'https://www.youtube.com/watch?v=B9FzVhw8_bY'  # Dead South - In Hell
    yt = BaseYT(url)
    yt.play_stream()
