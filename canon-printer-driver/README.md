# Installation of Canon printer driver for Linux

Official Ubuntu 20.04 does not have a suitable driver for my
Canon PIXMA MX410 (MX-410, MX 410), so I should have dug around for
a solution.


### Canon Driver

Download the driver from the
[Canon official repo:](https://www.canon-europe.com/support/consumer_products/products/fax__multifunctionals/inkjet/pixma_mx_series/pixma_mx410.html?type=drivers&language=en&os=linux%20(64-bit)#disclaimer)
Extract it and run `install.sh`, which is inside.

> Note, that I included a copy to this repo as well, but it may become obsolete.

### Troubleshooting

Most probably, it will throw dependency errors similar to these:
```bash
dpkg: dependency problems prevent configuration of cnijfilter-mx410series:
 cnijfilter-mx410series depends on libtiff4; however:
  Package libtiff4 is not installed.

dpkg: error processing package cnijfilter-mx410series (--install):
 dependency problems - leaving unconfigured
Errors were encountered while processing:
 cnijfilter-mx410series
```
These dependencies are not supported any more by official Ubuntu and must
be obtained elsewhere.

See bellow the solution, which worked for me.

##### libpng12-0
The `libpng12-0` package can be installed from a private repository:
```bash
sudo add-apt-repository ppa:linuxuprising/libpng12
sudo apt update
sudo apt install libpng12-0
```
This info was found
[here](https://www.linuxuprising.com/2018/05/fix-libpng12-0-missing-in-ubuntu-1804.html).

##### libtiff4
I tried to install several different versions of the `libtiff4` but
the only version, which worked for this driver was
`libtiff4_3.9.2-2ubuntu0.16_amd64.deb`, and can be downloaded from
[here](https://drive.google.com/drive/folders/1DUKKvui64R5gqVdhxYn-31y8HXmP8lxe).

> Note, that I included a copy to this repo as well, but it may become obsolete.

This package itself will require `libjpeg62`, which is available and can
be installed with `apt`:
```bash
sudo apt install libjpeg62
```
Then use `dpkg` to install the `libtiff4` package:
```bash
sudo dpkg -i libtiff4_3.9.2-2ubuntu0.16_amd64.deb
```

##### Dependencies conflicts
It may happen, that there are already some versions of the `libtiff4` and
`libpng12` installed on your system, then it will complain about the
conflicts. The resolution of those conflicts is usually proposed by `apt`
and is simple:
```bash
sudo apt --fix-broken install
```

##### Conclusion
After the troubleshooting steps above, the installation of the driver should
be performed smoothly. It will prompt for some printer details, which are
automatically detected and proposed as the default values.


### Collaboration

If you find another solution for this problem or you have an improvement
for this setup, feel free to send a merge request.
